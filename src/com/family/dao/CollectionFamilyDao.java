package com.family.dao;

import com.family.controller.FamilyController;
import com.family.model.Family;
import com.family.model.Human;
import com.family.model.Pet;
import com.family.service.FamilyService;

import java.io.*;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class CollectionFamilyDao implements FamilyDao {
    private List<Family> families;

    public CollectionFamilyDao() {
        this.families = new ArrayList<>();
    }

    @Override
    public List<Family> getAllFamilies() {
        return families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        if (index >= 0 && index < families.size()) {
            return families.get(index);
        }
        return null;
    }

    @Override
    public boolean deleteFamily(int index) {
        if (index >= 0 && index < families.size()) {
            families.remove(index);
            return true;
        }
        return false;
    }

    @Override
    public boolean saveFamily(Family family) {
        int index = families.indexOf(family);
        if (index != -1) {
            families.set(index, family);
        } else {
            families.add(family);
        }
        return true;
    }

    @Override
    public List<Family> getFamiliesBiggerThan(int count) {
        return families.stream()
                .filter(family -> family.countFamily() > count)
                .collect(Collectors.toList());
    }

    @Override
    public List<Family> getFamiliesLessThan(int count) {
        return families.stream()
                .filter(family -> family.countFamily() < count)
                .collect(Collectors.toList());
    }

    @Override
    public int countFamiliesWithMemberNumber(int count) {
        return (int) families.stream()
                .filter(family -> family.countFamily() == count)
                .count();
    }

    @Override
    public void displayAllFamilies() {
        for (Family family : families) {
            System.out.println(family);
        }
    }

    @Override
    public Family createNewFamily(Human mother, Human father) {
        Family newFamily = new Family(mother, father);
        families.add(newFamily);
        return newFamily;
    }

    @Override
    public Family bornChild(int familyIndex, String maleName, String femaleName) {
        Family family = getFamilyByIndex(familyIndex);
        if (family == null) {
            System.out.println("Family not found!");
            return null;
        }

        Random random = new Random();
        Human child;
        if (random.nextBoolean()) {
            child = new Human(maleName, family.getFather().getSurname(), 2023);
        } else {
            child = new Human(femaleName, family.getFather().getSurname(), 2023);
        }

        family.addChild(child);
        saveFamily(family);
        return family;
    }

    @Override
    public Family adoptChild(Family family, Human child) {
        family.getChildren().add(child);
        saveFamily(family);
        return family;
    }

    @Override
    public boolean deleteAllChildrenOlderThan(int age) {
        long currentMillis = System.currentTimeMillis();
        LocalDate currentDate = LocalDate.ofEpochDay(currentMillis / (24 * 60 * 60 * 1000));
        List<Family> updatedFamilies = new ArrayList<>();
        for (Family family : families) {
            List<Human> updatedChildren = family.getChildren().stream()
                    .filter(child -> calculateAge(child.getBirthDate(), currentDate) <= age)
                    .collect(Collectors.toList());
            family.setChildren(updatedChildren);
            updatedFamilies.add(family);
        }
        families = updatedFamilies;
        return true;
    }

    private int calculateAge(long birthDateMillis, LocalDate currentDate) {
        LocalDate birthDate = LocalDate.ofEpochDay(birthDateMillis / (24 * 60 * 60 * 1000));
        return Period.between(birthDate, currentDate).getYears();
    }

    @Override
    public int count() {
        return families.size();
    }

    @Override
    public Family getFamilyById(int id) {
        return getFamilyByIndex(id);
    }

    @Override
    public List<Pet> getPets(int familyIndex) {
        Family family = getFamilyByIndex(familyIndex);
        return family != null ? new ArrayList<>(family.getPets()) : Collections.emptyList();
    }

    @Override
    public boolean addPets(int familyIndex, Pet pet) {
        Family family = getFamilyByIndex(familyIndex);
        if (family != null) {
            family.getPets().add(pet);
            return saveFamily(family);
        } else {
            return false;
        }
    }

    @Override
    public void loadDataFromFile(String fileName) throws IOException, ClassNotFoundException {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            families.clear();
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

            String line;
            while ((line = reader.readLine()) != null) {
                String[] data = line.split(",");
                if (data.length == 6) {
                    String motherName = data[0];
                    String motherSurname = data[1];
                    LocalDate motherBirthDate = LocalDate.parse(data[2], dateFormatter);
                    String fatherName = data[3];
                    String fatherSurname = data[4];
                    LocalDate fatherBirthDate = LocalDate.parse(data[5], dateFormatter);

                    Human mother = new Human(motherName, motherSurname, motherBirthDate.toEpochDay());
                    Human father = new Human(fatherName, fatherSurname, fatherBirthDate.toEpochDay());

                    Family family = new Family(mother, father);
                    families.add(family);
                } else if (data.length == 3) {
                    String childName = data[0];
                    String childSurname = data[1];
                    LocalDate childBirthDate = LocalDate.parse(data[2], dateFormatter);

                    if (!families.isEmpty()) {
                        families.get(families.size() - 1).addChild(new Human(childName, childSurname, childBirthDate.toEpochDay()));
                    }
                }
            }
        }
        System.out.println("Дані завантажено з файлу " + fileName);
    }

    @Override
    public void saveDataToFile(String fileName) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

            for (Family family : families) {
                Human mother = family.getMother();
                Human father = family.getFather();
                String familyInfo = mother.getName() + "," + mother.getSurname() + "," +
                        dateFormatter.format(LocalDate.ofEpochDay(mother.getBirthDate())) + "," +
                        father.getName() + "," + father.getSurname() + "," +
                        dateFormatter.format(LocalDate.ofEpochDay(father.getBirthDate()));
                writer.write(familyInfo);
                writer.newLine();

                for (Human child : family.getChildren()) {
                    String childInfo = child.getName() + "," + child.getSurname() + "," +
                            dateFormatter.format(LocalDate.ofEpochDay(child.getBirthDate()));
                    writer.write(childInfo);
                    writer.newLine();
                }
                writer.write("///");
                writer.newLine();
            }
        }
        System.out.println("Дані збережено у файл " + fileName);
    }
}