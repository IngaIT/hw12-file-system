package com.family.service;

import com.family.dao.FamilyDao;
import com.family.model.Family;
import com.family.model.Human;
import com.family.model.Pet;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FamilyService {
    private FamilyDao familyDao;
    private List<Family> families;

    /*public FamilyService() {
        this.families = new ArrayList<>();
    }*/

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
        this.families = new ArrayList<>();
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyDao.getAllFamilies().forEach(family -> System.out.println(family.prettyFormat()));
    }

    public List<Family> getFamiliesBiggerThan(int count) {
        return familyDao.getAllFamilies().stream()
                .filter(family -> family.countFamily() > count)
                .collect(Collectors.toList());
    }

    public List<Family> getFamiliesLessThan(int numberOfPeople) {
        return familyDao.getAllFamilies().stream()
                .filter(family -> family.countFamily() < numberOfPeople)
                .collect(Collectors.toList());
    }

    public int countFamiliesWithMemberNumber(int numberOfPeople) {
        return (int) familyDao.getAllFamilies().stream()
                .filter(family -> family.countFamily() == numberOfPeople)
                .count();
    }

    public Family createNewFamily(Human mother, Human father) {
        return familyDao.createNewFamily(mother, father);
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyDao.deleteFamily(index);
    }

    public Family adoptChild(Family family, Human child) {
        family.getChildren().add(child);
        familyDao.saveFamily(family);
        return family;
    }

    public int count() {
        return familyDao.count();
    }

    public Family getFamilyById(int index) {
        return familyDao.getFamilyById(index);
    }

    public List<Pet> getPets(int index) {
        return familyDao.getPets(index);
    }

    public boolean addPets(int index, Pet pet) {
        return familyDao.addPets(index, pet);
    }

    public Family bornChild(int family, String maleName, String femaleName) {
        return familyDao.bornChild(family, maleName, femaleName);
    }

    public void deleteAllChildrenOlderThan(int age) {
        familyDao.getAllFamilies().forEach(family -> family.getChildren().removeIf(child -> child.describeAge().startsWith(age + " years")));
    }
    public boolean saveFamily(Family family) {
        return familyDao.saveFamily(family);
    }

    public void adoptChild(int familyIndexAdopt, String childNameAdopt, String childGenderAdopt) {


    }

    public Family getFamilyByIndex(int familyIndexBorn) {


        return null;
    }

    public void saveDataToFile(String fileName) throws IOException {
        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(fileName))) {
            outputStream.writeObject(families);
        }
    }

    public void loadDataFromFile(String fileName) throws IOException, ClassNotFoundException {
        try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(fileName))) {
            List<Family> loadedFamilies = (List<Family>) inputStream.readObject();
            this.families.clear();
            this.families.addAll(loadedFamilies);
        }
    }

}

