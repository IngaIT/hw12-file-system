package com.family;

import com.family.controller.FamilyController;
import com.family.dao.CollectionFamilyDao;
import com.family.dao.FamilyDao;
import com.family.model.*;
import com.family.service.FamilyService;

import java.io.*;
import java.util.*;

public class Main {
    private static final Scanner scanner = new Scanner(System.in);
    private static final String FILE_NAME = "data.txt";

    public static void main(String[] args) {
        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);

        boolean exit = false;

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {// Загружаем ранее сохраненные данные
            while (!exit) {
                System.out.println("\nДоступні команди:");
                System.out.println("1. Відобразити весь список сімей");
                System.out.println("2. Відобразити список сімей, де кількість людей більша за задану");
                System.out.println("3. Відобразити список сімей, де кількість людей менша за задану");
                System.out.println("4. Підрахувати кількість сімей, де кількість членів дорівнює");
                System.out.println("5. Створити нову родину");
                System.out.println("6. Видалити сім'ю за індексом сім'ї у загальному списку");
                System.out.println("7. Редагувати сім'ю за індексом сім'ї у загальному списку");
                System.out.println("8. Видалити всіх дітей старше віку (у всіх сім'ях)");
                System.out.println("9. Зберегти дані");
                System.out.println("10. Завантажити раніше збережені дані");
                System.out.println("0. Вийти з програми");

                System.out.print("Введіть номер команди: ");
                int choice = Integer.parseInt(reader.readLine());

                switch (choice) {
                    case 1:
                        displayAllFamilies(familyController);
                        break;
                    case 2:
                        displayFamiliesBiggerThan(familyController, reader);
                        break;
                    case 3:
                        displayFamiliesLessThan(familyController, reader);
                        break;
                    case 4:
                        countFamiliesWithMemberNumber(familyController, reader);
                        break;
                    case 5:
                        createNewFamily(familyController, reader);
                        break;
                    case 6:
                        deleteFamilyByIndex(familyController, reader);
                        break;
                    case 7:
                        editFamily(familyController, reader);
                        break;
                    case 8:
                        deleteChildrenOlderThan(familyController, reader);
                        break;
                    case 9:
                        saveData(familyController, reader);
                        break;
                    case 10:
                        loadData(familyController, reader);
                        break;
                    case 0:
                        saveData(familyController, reader);
                        exit = true;
                        break;
                    default:
                        System.out.println("Невідома команда. Спробуйте ще раз.");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    private static void saveData(FamilyController familyController, BufferedReader reader) throws IOException {
        System.out.print("Введіть ім'я файлу для збереження даних: ");
        String fileName = reader.readLine();
        try {
            familyController.saveDataToFile(fileName);
            System.out.println("Дані збережено у файл " + fileName);
        } catch (IOException e) {
            System.out.println("Помилка збереження даних у файл " + fileName);
            e.printStackTrace();
        }
    }

    private static void loadData(FamilyController familyController, BufferedReader reader) throws IOException, ClassNotFoundException {
        System.out.print("Введіть ім'я файлу для завантаження даних: ");
        String fileName = reader.readLine();
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName))) {
            List<Family> loadedFamilies = (List<Family>) ois.readObject();
            familyController.loadDataFromFile(fileName); // Load data from the specified file instead of "data.txt"
            System.out.println("Дані завантажено з файлу " + fileName);
        } catch (IOException e) {
            System.out.println("Помилка завантаження даних з файлу " + fileName);
            e.printStackTrace();
        }
    }

    private static void displayAllFamilies(FamilyController familyController) {
        System.out.println("\nСписок усіх сімей:");
        familyController.displayAllFamilies();
    }

    private static void displayFamiliesBiggerThan(FamilyController familyController, BufferedReader reader) throws IOException {
        System.out.print("\nВведіть цікаву кількість людей: ");
        int count = Integer.parseInt(reader.readLine());
        List<Family> families = familyController.getFamiliesBiggerThan(count);
        System.out.println("\nСписок сімей, де кількість людей більша за " + count + ":");
        families.forEach(family -> System.out.println(family.prettyFormat()));
    }

    private static void displayFamiliesLessThan(FamilyController familyController, BufferedReader reader) throws IOException {
        System.out.print("\nВведіть цікаву кількість людей: ");
        int count = Integer.parseInt(reader.readLine());
        List<Family> families = familyController.getFamiliesLessThan(count);
        System.out.println("\nСписок сімей, де кількість людей менша за " + count + ":");
        families.forEach(System.out::println);
    }

    private static void countFamiliesWithMemberNumber(FamilyController familyController, BufferedReader reader) throws IOException {
        System.out.print("\nВведіть цікаву кількість членів сім'ї: ");
        int count = Integer.parseInt(reader.readLine());
        int numFamilies = familyController.countFamiliesWithMemberNumber(count);
        System.out.println("Кількість сімей, де кількість членів дорівнює " + count + ": " + numFamilies);
    }

    private static void createNewFamily(FamilyController familyController, BufferedReader reader) throws IOException {
        System.out.println("\nСтворення нової родини:");

        System.out.print("Введіть ім'я матері: ");
        String motherName = reader.readLine();

        System.out.print("Введіть прізвище матері: ");
        String motherSurname = reader.readLine();

        System.out.print("Введіть рік народження матері: ");
        int motherBirthYear = Integer.parseInt(reader.readLine());

        System.out.print("Введіть місяць народження матері: ");
        int motherBirthMonth = Integer.parseInt(reader.readLine());

        System.out.print("Введіть день народження матері: ");
        int motherBirthDay = Integer.parseInt(reader.readLine());

        System.out.print("Введіть iq матері: ");
        int motherIq = Integer.parseInt(reader.readLine());

        System.out.print("Введіть ім'я батька: ");
        String fatherName = reader.readLine();

        System.out.print("Введіть прізвище батька: ");
        String fatherSurname = reader.readLine();

        System.out.print("Введіть рік народження батька: ");
        int fatherBirthYear = Integer.parseInt(reader.readLine());

        System.out.print("Введіть місяць народження батька: ");
        int fatherBirthMonth = Integer.parseInt(reader.readLine());

        System.out.print("Введіть день народження батька: ");
        int fatherBirthDay = Integer.parseInt(reader.readLine());

        System.out.print("Введіть iq батька: ");
        int fatherIq = Integer.parseInt(reader.readLine());

        Human mother = new Human(motherName, motherSurname, motherBirthYear, motherIq, null);
        Human father = new Human(fatherName, fatherSurname, fatherBirthYear, fatherIq, null);

        Family newFamily = familyController.createNewFamily(mother, father);
        System.out.println("\nНова родина створена:");
        System.out.println(newFamily.prettyFormat());
    }

    private static void deleteFamilyByIndex(FamilyController familyController, BufferedReader reader) throws IOException {
        System.out.print("\nВведіть індекс сім'ї, яку бажаєте видалити: ");
        int index = Integer.parseInt(reader.readLine());
        if (familyController.deleteFamilyByIndex(index - 1)) {
            System.out.println("Сім'ю за індексом " + index + " видалено успішно.");
        } else {
            System.out.println("Сім'ю за індексом " + index + " не знайдено.");
        }
    }

    private static void editFamily(FamilyController familyController, BufferedReader reader) throws IOException {
        System.out.print("\nВведіть індекс сім'ї, яку бажаєте редагувати: ");
        int index = Integer.parseInt(reader.readLine());

        boolean backToMainMenu = false;
        while (!backToMainMenu) {
            System.out.println("\nРедагування сім'ї:");
            System.out.println("1. Народити дитину");
            System.out.println("2. Усиновити дитину");
            System.out.println("3. Повернутися до головного меню");

            System.out.print("Введіть номер команди: ");
            int choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    bornChild(familyController, index);
                    break;
                case 2:
                    adoptChild(familyController, index);
                    break;
                case 3:
                    backToMainMenu = true;
                    break;
                default:
                    System.out.println("Невідома команда. Спробуйте ще раз.");
            }
        }
    }

    private static void bornChild(FamilyController familyController, int familyIndex) {
        System.out.print("Введіть ім'я дитини (хлопчика): ");
        String boyName = scanner.next();
        System.out.print("Введіть ім'я дитини (дівчинки): ");
        String girlName = scanner.next();
        Family family = familyController.getFamilyById(familyIndex - 1);
        familyController.bornChild(family, boyName, girlName);
        System.out.println("\nДитину успішно народжено:");
        System.out.println(family.prettyFormat());
    }

    private static void adoptChild(FamilyController familyController, int familyIndex) {
        scanner.nextLine();
        System.out.print("Введіть ПІБ дитини: ");
        String childName = scanner.nextLine();
        System.out.print("Введіть рік народження дитини: ");
        int childBirthYear = scanner.nextInt();
        System.out.print("Введіть інтелект дитини: ");
        int childIq = scanner.nextInt();

        Family family = familyController.getFamilyById(familyIndex - 1);
        Human child = new Human(childName, family.getFather().getSurname(), childBirthYear, childIq, null);
        familyController.adoptChild(family, child);

        System.out.println("\nДитину успішно усиновлено:");
        System.out.println(family.prettyFormat());
    }

    private static void deleteChildrenOlderThan(FamilyController familyController, BufferedReader reader) throws IOException {
        System.out.print("Введіть вік, старший за який бажаєте видалити дітей: ");
        int age = Integer.parseInt(reader.readLine());
        familyController.deleteAllChildrenOlderThan(age);
        System.out.println("\nВсі діти старше " + age + " років видалені.");
    }
}


