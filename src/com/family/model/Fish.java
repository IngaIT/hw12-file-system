package com.family.model;

import java.util.Set;

public class Fish extends Pet {
    public Fish(String nickname) {
        super(nickname);
        setSpecies(Pet.Species.FISH);
    }

    public Fish(Pet.Species species, String nickname, int age, int trickLevel, Set<String> habits) {
        super(species, nickname, age, trickLevel, habits);
    }

    public Fish() {
    }

    @Override
    public void respond(){
        System.out.println("Привіт, я риба " + this.getNickname());
    }
}
